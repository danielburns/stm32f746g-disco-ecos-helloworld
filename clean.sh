#!/bin/bash

TARGETS="$@"

if [ "$TARGETS" = "" ]; then
    # default target is stm32f746g-disco-ecos-helloworld-rel
    TARGETS="stm32f746g-disco-ecos-helloworld-rel"
fi

if [ "$TARGETS" = "all" ]; then
    # all builds release and debug
    TARGETS="stm32f746g-disco-ecos-helloworld-rel ecos"
fi

for TARGET in $TARGETS; do

    if [ "$TARGET" = "stm32f746g-disco-ecos-helloworld-rel" ]; then

        echo "cleaning stm32f746g-disco-ecos-helloworld-rel..."

        # clean the build directory (old artifacts)
        if [ -d "build" ]; then
            rm -f -r "build"
        fi

        # clean any existing debug & release directories
        if [ -d "stm32f746g-disco-ecos-helloworld-dbg" ]; then
            rm -f -r "stm32f746g-disco-ecos-helloworld-dbg"
        fi
        if [ -d "stm32f746g-disco-ecos-helloworld-rel" ]; then
            rm -f -r "stm32f746g-disco-ecos-helloworld-rel"
        fi

    elif [ "$TARGET" = "ecos" ]; then

        echo "cleaning ecos..."

        # source the ecos environment
        . $(realpath "$(dirname "$0")")/.ecos-env

        # clean the ecos build
        if [ -d "$ECOS_BUILD_PATH" ]; then
            rm -f -r "$ECOS_BUILD_PATH"
        fi
        if [ -d "$ECOS_INSTALL_PATH" ]; then
            rm -f -r "$ECOS_INSTALL_PATH"
        fi
    fi

done
