#!/bin/bash
#Post build for SECBOOT_ECCDSA_WITH_AES128_CBC_SHA256
# arg1 is the build directory
# arg2 is the elf file path+name
# arg3 is the bin file path+name
# arg4 is the firmware Id (1/2/3)
# arg5 is the version
# arg6 when present forces "bigelf" generation
buildDir=$1
binFileBasename=${3##*/}
fileBasename=${binFileBasename%.*}
elfFile=$2
binFile=$3
fwid=$4
version=$5

projectPath=${0%/*} 

outputPath=$buildDir

sfuFile=$outputPath"/"$fileBasename".sfu"
sfbFile=$outputPath"/"$fileBasename".sfb"
signFile=$outputPath"/"$fileBasename".sign"
headerFile=$outputPath"/"$fileBasename"sfuh.bin"
binAndSbsfuFile=$outputPath"/SBSFU_"$fileBasename".bin"

ivFile=$projectPath"/sbsfu/secorebin_binary/iv.bin"
magic="SFU"$fwid
oemKeyFile=$projectPath"/sbsfu/secorebin_binary/OEM_KEY_COMPANY"$fwid"_key_AES_CBC.bin"
ecckey=$projectPath"/sbsfu/secorebin_binary/ECCKEY"$fwid".txt"
offset=1024

sbsfuElf=$projectPath"/sbsfu/sbsfu_debug/SBSFU.elf"

prepareImage=$projectPath"/sbsfu/middlewares_st_stm32_secure_engine_utilities_keysandimages/prepareimage.py"

echo "$cmd $prepareImage"
# Make sure we have a Binary sub-folder in UserApp folder
if [ ! -e $outputPath ]; then
  mkdir $outputPath
fi

command=$cmd" "$prepareImage" enc -k "$oemKeyFile" -i "$ivFile" "$binFile" "$sfuFile
echo "command = $command..."
$command
ret=$?
if [ $ret -eq 0 ]; then
  echo "command = $command..."
  command=$cmd" "$prepareImage" sha256 "$binFile" "$signFile
  $command
  ret=$?
  if [ $ret -eq 0 ]; then 
    command=$cmd" "$prepareImage" pack -m "$magic" -k "$ecckey" -r 28 -v "$version" -i "$ivFile" -f "$sfuFile" -t "$signFile" "$sfbFile" -o "$offset
    echo "command = $command..."
    $command
    ret=$?
    if [ $ret -eq 0 ]; then
      command=$cmd" "$prepareImage" header -m "$magic" -k  "$ecckey" -r 28 -v "$version"  -i "$ivFile" -f "$sfuFile" -t "$signFile" -o "$offset" "$headerFile
      echo "command = $command..."
      $command
      ret=$?
      if [ $ret -eq 0 ]; then
        command=$cmd" "$prepareImage" merge -v 0 -e 1 -i "$headerFile" -s "$sbsfuElf" -u "$elfFile" "$binAndSbsfuFile
        echo "command = $command..."
        $command
        ret=$?
        if [ $ret -eq 0 ] && [ $# = 6 ]; then
          echo "Generating the global elf file SBSFU and userApp"
          echo "Generating the global elf file SBSFU and userApp"
          uname | grep -i -e windows -e mingw > /dev/null 2>&1
          if [ $? -eq 0 ]; then
            # Set to the default installation path of the Cube Programmer tool
            # If you installed it in another location, please update PATH.
            PATH="C:\\Program Files (x86)\\STMicroelectronics\\STM32Cube\\STM32CubeProgrammer\\bin":$PATH > /dev/null 2>&1
            programmertool="STM32_Programmer_CLI.exe"
          else
            which STM32_Programmer_CLI > /dev/null
            if [ $? = 0 ]; then
              programmertool="STM32_Programmer_CLI"
            else
              echo "fix access path to STM32_Programmer_CLI"
            fi
          fi
          command=$programmertool" -ms "$elfFile" "$headerFile" "$sbsfuElf
          echo "command = $command..."
          $command
          ret=$?
        fi
      fi
    fi
  fi
fi

if [ $ret -eq 0 ]; then
  echo "remove files..."
  rm $signFile
  rm $sfuFile
  rm $headerFile
  exit 0
else 
  echo "$command : failed"
  if [ -e  "$elfFile" ]; then
    rm  $elfFile
  fi
  if [ -e "$elfbackup" ]; then 
    rm  $elfbackup
  fi
  echo $command : failed
  #read -n 1 -s
  exit 1
fi
