#!/bin/bash

# configure the project if not already configured
if [ "$PROJECT_NAME" = "" ]; then
    . $(realpath "$(dirname "$0")")/.configure $@
fi

# enter the build directory
cd $PROJECT_NAME

# build the configuration
make -j8 VERBOSE=1

# display build output
ls -l

# leave the build directory
cd -
