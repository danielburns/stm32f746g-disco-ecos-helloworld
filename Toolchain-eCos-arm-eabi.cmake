# the name of the target operating system
SET(CMAKE_SYSTEM_NAME eCos)

# which compiler to use
set(CMAKE_C_COMPILER arm-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-eabi-g++)

# where is the target environment located
SET(CMAKE_FIND_ROOT_PATH $ENV{ECOS_INSTALL_PATH})
message("CMAKE_FIND_ROOT_PATH=" ${CMAKE_FIND_ROOT_PATH})

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(COMMON_COMPILER_FLAGS "-Wall -fmessage-length=0 -Wpointer-arith -Wno-write-strings -mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections -fno-exceptions")

# instead of setting the flags init variables, we want to clear out the cached flags variables
#set(CMAKE_CXX_FLAGS_DEBUG_INIT   "-O0 -g ${COMMON_COMPILER_FLAGS}")
#set(CMAKE_CXX_FLAGS_RELEASE_INIT "-Os ${COMMON_COMPILER_FLAGS}")
#set(CMAKE_C_FLAGS_DEBUG_INIT     "-O0 -g ${COMMON_COMPILER_FLAGS}")
#set(CMAKE_C_FLAGS_RELEASE_INIT   "-Os ${COMMON_COMPILER_FLAGS}")

UNSET(CMAKE_CXX_FLAGS_DEBUG CACHE)
set(CMAKE_CXX_FLAGS_DEBUG   "-O0 -g ${COMMON_COMPILER_FLAGS} -DDEBUG_LOGGING" CACHE STRING "" FORCE)
UNSET(CMAKE_CXX_FLAGS_RELEASE CACHE)
set(CMAKE_CXX_FLAGS_RELEASE "-Os ${COMMON_COMPILER_FLAGS}" CACHE STRING "" FORCE)
UNSET(CMAKE_C_FLAGS_DEBUG CACHE)
set(CMAKE_C_FLAGS_DEBUG     "-O0 -g ${COMMON_COMPILER_FLAGS} -DDEBUG_LOGGING" CACHE STRING "" FORCE)
UNSET(CMAKE_C_FLAGS_RELEASE CACHE)
set(CMAKE_C_FLAGS_RELEASE   "-Os ${COMMON_COMPILER_FLAGS}" CACHE STRING "" FORCE)

set(COMMON_LINKER_FLAGS "-mcpu=cortex-m3 -Wl,--gc-sections -Wl,-static -Wl,-n -g -mthumb -Xlinker --gc-sections -Xlinker -M -Xlinker -Map -Xlinker test_dbg.map")

# instead of setting the flags init variables, we want to clear out the cached flags variables
#set(CMAKE_EXE_LINKER_FLAGS_DEBUG_INIT "${COMMON_LINKER_FLAGS}")
#set(CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT "${COMMON_LINKER_FLAGS}")

UNSET(CMAKE_EXE_LINKER_FLAGS_DEBUG CACHE)
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${COMMON_LINKER_FLAGS}" CACHE STRING "" FORCE)
UNSET(CMAKE_EXE_LINKER_FLAGS_RELEASE CACHE)
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${COMMON_LINKER_FLAGS}" CACHE STRING "" FORCE)
