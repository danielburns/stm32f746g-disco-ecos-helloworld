# setup
```
Use Ubuntu.20.04.02_64-bit_eCos-3.1.96 VM
sudo apt install python3-pycryptodome
sudo apt install python3-ecdsa
sudo apt install python3-numpy
sudo apt install python3-pyelftools
```

# ecos
```
cd ~/git/ecos
git fetch
git checkout origin/stm32f746g-disco -b stm32f746g-disco
```

# build
```
./clean.sh
./build.sh
```
