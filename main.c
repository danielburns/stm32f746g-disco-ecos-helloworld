#define INCLUDE_KAPI
#ifdef INCLUDE_KAPI
#include <cyg/kernel/kapi.h>
#include <cyg/infra/diag.h>
#endif

//#define INCLUDE_MAIN
#ifdef INCLUDE_MAIN
extern int main(int argc, char** argv)
{
    #ifdef INCLUDE_KAPI
    diag_printf("\r\n\r\nmain:: hello world\r\n");
    cyg_thread_delay(10000);
    #endif
    for (;;) ;
    return 0;
}
#endif

extern void cyg_user_start(void)
{
    #ifdef INCLUDE_KAPI
    diag_printf("\r\n\r\necos::cyg_user_start:: hello world!\r\n");
    cyg_thread_delay(10000);
    #endif
    for (;;) ;
    return;
}
